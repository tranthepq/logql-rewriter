package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
	"time"

	"golang.org/x/time/rate"
)

type mockAuthorizer struct {
	test func(r *http.Request) error
}

func (m *mockAuthorizer) Authorize(ctx context.Context, r *http.Request, limit *rate.Limiter) error {
	if m.test == nil {
		return nil
	}
	return m.test(r)
}

const (
	username = "username"
	password = "password"
)

var basicMockAuthorizer = &mockAuthorizer{
	test: func(r *http.Request) error {
		user, pass, ok := r.BasicAuth()
		if !ok {
			return fmt.Errorf("basic auth: could not get username/password from request")
		}
		if user == username && pass == password {
			return nil
		}
		return fmt.Errorf("authentication failed")
	},
}

type jsonTime time.Time

func (j *jsonTime) UnmarshalJSON(b []byte) error {
	var n json.Number
	if err := json.Unmarshal(b, &n); err != nil {
		return err
	}
	var unix int64

	if t, err := n.Int64(); err == nil {
		unix = t
	} else {
		f, err := n.Float64()
		if err != nil {
			return err
		}
		unix = int64(f)
	}
	*j = jsonTime(time.Unix(unix, 0))
	return nil
}

type idToken struct {
	Issuer    string    `json:"iss"`
	Subject   string    `json:"sub"`
	Audience  []string  `json:"aud"`
	Expiry    jsonTime  `json:"exp"`
	IssuedAt  jsonTime  `json:"iat"`
	NotBefore *jsonTime `json:"nbf"`
	Nonce     string    `json:"nonce"`
	AtHash    string    `json:"at_hash"`
}

var jwtMockAuthorizer = &mockAuthorizer{
	test: func(r *http.Request) error {
		data, err := parseJWT(r.Header.Get(authorizationHeaderName))
		if err != nil {
			return err
		}
		// Let's see if we can get a token out of this.
		var token idToken
		if err := json.Unmarshal(data, &token); err != nil {
			return fmt.Errorf("oidc: failed to unmarshal claims: %v", err)
		}
		exp := time.Time(token.Expiry)
		if exp.After(iat()) {
			return nil
		}
		return fmt.Errorf("oidc: token expired at %s", exp)
	},
}

// From go-oidc lib.
func parseJWT(p string) ([]byte, error) {
	parts := strings.Split(p, ".")
	if len(parts) < 2 {
		return nil, fmt.Errorf("oidc: malformed jwt, expected 3 parts got %d", len(parts))
	}
	payload, err := base64.RawURLEncoding.DecodeString(parts[1])
	if err != nil {
		return nil, fmt.Errorf("oidc: malformed jwt payload: %v", err)
	}
	return payload, nil
}

var _ authorizer = &mockAuthorizer{}

func validRawAccessToken() []byte {
	data, err := ioutil.ReadFile("testdata/valid-token.txt")
	if err != nil {
		panic(err)
	}
	return data
}

// iat returns always the same time for testing.
var iat = func() time.Time {
	return time.Unix(1615690023, 0) // Issuance of the test token.
}

func Test_switchingAuthorizer_Authorize(t *testing.T) {
	var newRequest = func() *http.Request {
		r, err := http.NewRequest(http.MethodGet, "", nil)
		if err != nil {
			t.Error(err)
			return nil
		}
		return r
	}
	type wantFunc func(*http.Request) error
	type fields struct {
		basic              authorizer
		jwt                authorizer
		removeTokenHeaders []string
	}
	var stdFields = fields{
		basic:              basicMockAuthorizer,
		jwt:                jwtMockAuthorizer,
		removeTokenHeaders: []string{authorizationHeaderName},
	}
	type args struct {
		ctx context.Context
		rf  func() *http.Request
		lim *rate.Limiter
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		want    wantFunc
	}{
		{
			name:   "basic auth success",
			fields: stdFields,
			args: args{
				ctx: context.TODO(),
				rf: func() *http.Request {
					r := newRequest()
					r.SetBasicAuth(username, password)
					return r
				},
			},
			wantErr: false,
		},
		{
			name:   "basic auth wrong credentials",
			fields: stdFields,
			args: args{
				ctx: context.TODO(),
				rf: func() *http.Request {
					r := newRequest()
					r.SetBasicAuth("meh", "meh")
					return r
				},
			},
			wantErr: true,
		},
		{
			name:   "no credentials at all",
			fields: stdFields,
			args: args{
				ctx: context.TODO(),
			},
			wantErr: true,
		},
		{
			name:   "remove auth header",
			fields: stdFields,
			args: args{
				ctx: context.TODO(),
				rf: func() *http.Request {
					r := newRequest()
					r.SetBasicAuth(username, password)
					return r
				},
			},
			wantErr: false,
			want: func(r *http.Request) error {
				if hdr := r.Header.Get(authorizationHeaderName); hdr != "" {
					return fmt.Errorf("header %s still set: %s", authorizationHeaderName, hdr)
				}
				return nil
			},
		},
		{
			name:   "jwt auth success",
			fields: stdFields,
			args: args{
				ctx: context.TODO(),
				rf: func() *http.Request {
					r := newRequest()
					r.Header.Set(authorizationHeaderName, "Bearer "+string(validRawAccessToken()))
					return r
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := switchingAuthorizer{
				basic:              tt.fields.basic,
				jwt:                tt.fields.jwt,
				removeTokenHeaders: tt.fields.removeTokenHeaders,
			}
			var r *http.Request = newRequest()
			if tt.args.rf != nil {
				r = tt.args.rf()
			}
			if err := s.authorize(tt.args.ctx, r, tt.args.lim); (err != nil) != tt.wantErr {
				t.Errorf("switchingAuthorizer.Authorize() error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.want != nil {
				if err := tt.want(r); err != nil {
					t.Error(err)
				}
			}
		})
	}
}
