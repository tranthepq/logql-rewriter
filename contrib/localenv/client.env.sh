#!/bin/sh

# Source this file to test the client locally agains the keycloak instance.

export LOGQL_CLIENT_ID=logql
export LOGQL_CLIENT_SECRET=82cbaf60-00c7-43d9-a99a-49840affa92b
export LOGQL_IDP=http://localhost:8180/auth/realms/myrealm
export LOGQL_REDIRECT_URL=http://localhost:8080/
export LOGQL_UPSTREAM=http://localhost:9090/
#export LOGQL_SCOPE=""

export TOKEN=$(curl -X POST -s -d 'grant_type=password&scope=openid email profile&client_id=logql&client_secret=82cbaf60-00c7-43d9-a99a-49840affa92b&username=myuser&password=test' \
		-H "Content-Type: application/x-www-form-urlencoded" \
		http://localhost:8180/auth/realms/myrealm/protocol/openid-connect/token | jq -j '.access_token')