package fastly

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"path"
	"strings"
	"testing"

	"github.com/go-test/deep"
)

func TestHandler_handleChallenge(t *testing.T) {
	type wantFunc func(http.Handler) error
	type fields struct {
		services []string
	}
	tests := []struct {
		name    string
		fields  fields
		want    wantFunc
		wantErr bool
	}{
		{
			name: "check service id hashing",
			fields: fields{
				services: []string{"testservice", "*"},
			},
			want: func(h http.Handler) error {
				req, err := http.NewRequest(http.MethodGet, path.Join(rfc8615prefix, challengePath), nil)
				if err != nil {
					t.Fatal(err)
				}
				rr := httptest.NewRecorder()
				h.ServeHTTP(rr, req)
				if status := rr.Code; status != http.StatusOK {
					return fmt.Errorf("code got = %d, want %v", status, http.StatusOK)
				}
				want := "fdcc848c4c1800dc6f24a26f95804e5af40c1afe4740c3fb66523f145af2216b\n*"
				got := strings.TrimSpace(rr.Body.String())
				if diff := deep.Equal(want, got); diff != nil {
					return fmt.Errorf("%v", diff)
				}
				return nil
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			h, err := NewHandler(
				http.NotFoundHandler(),
				WithServiceIDs(tt.fields.services...),
			)
			if (err != nil) != tt.wantErr {
				t.Error(err)
			}
			if tt.want == nil {
				return
			}
			if err := tt.want(h); err != nil {
				t.Error(err)
			}
		})
	}
}
