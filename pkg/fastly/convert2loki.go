package fastly

import (
	"encoding/json"
	"fmt"
	"io"
	"sort"
	"strconv"
	"time"
)

// FastlyLogLine is something fastly sends us. This has to be configured in the
// dashboard (service).
type FastlyLogLine struct {
	Host        string `json:"host"`
	IP          string `json:"ip"`
	Date        string `json:"date"`
	Path        string `json:"path"`
	Method      string `json:"method"`
	Protocol    string `json:"protocol"`
	Status      int    `json:"status"`
	Bytes       int    `json:"bytes"`
	CacheStatus string `json:"cache_status"`
	RequestTime int64  `json:"request_time"`
	Zone        string `json:"zone"`
}

type LokiLabels struct {
	Tenant string `json:"tenant"`
	Host   string `json:"host"`
}

type LokiStream struct {
	Stream LokiLabels `json:"stream"`
	Values [][]string `json:"values"`
}

type LokiLogLine struct {
	Streams []LokiStream `json:"streams"`
}

var (
	insideTest bool
	testNow    = time.Date(2006, 02, 01, 03, 04, 05, 0, time.UTC)
)

func fastly2Loki(in io.Reader, tenant string) (*LokiLogLine, error) {
	fastlyLog := make([]FastlyLogLine, 0)
	if err := json.NewDecoder(in).Decode(&fastlyLog); err != nil {
		return nil, fmt.Errorf("could not parse fastly log: %w", err)
	}

	// We use this map to group by host.
	streams := make(map[string][][]string)

	// Keep the same timestamp for all log entries we received in one single
	// request. This is not a stream so we should be fine.
	//
	// Docs stating: > logs sent to Loki for every stream must be in
	// timestamp-ascending order; logs with identical timestamps are only
	// allowed if their content differs.
	//
	// https://grafana.com/docs/loki/latest/api/#post-lokiapiv1push
	now := time.Now().UnixNano()
	if insideTest {
		now = testNow.UnixNano()
	}
	for _, v := range fastlyLog {
		enc, err := json.Marshal(v)
		if err != nil {
			return nil, fmt.Errorf("could not marshal fastly log line to json: %w", err)
		}
		entry := []string{strconv.FormatInt(now, 10), string(enc)}
		streams[v.Host] = append(streams[v.Host], entry)
	}

	lokiLog := new(LokiLogLine)
	// Make sure output is deterministic.
	keys := make([]string, 0)
	for k := range streams {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		stream := LokiStream{
			Stream: LokiLabels{
				Tenant: tenant,
				Host:   k,
			},
			Values: streams[k],
		}
		lokiLog.Streams = append(lokiLog.Streams, stream)
	}

	return lokiLog, nil
}
