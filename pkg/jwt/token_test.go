package jwt

import (
	"io/ioutil"
	"net/http"
	"testing"
)

func fixture(name string, t *testing.T) []byte {
	data, err := ioutil.ReadFile("testdata/" + name)
	if err != nil {
		t.Fatal(err)
	}
	return data
}

func Test_authorizeMethod(t *testing.T) {
	type args struct {
		accessToken []byte
		method      string
		clientID    string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "get request allowed for logs",
			args: args{
				accessToken: fixture("accessToken.json", t),
				method:      http.MethodGet,
				clientID:    "logql",
			},
		},
		{
			name: "get request allowed for logs",
			args: args{
				accessToken: fixture("accessTokenRead.json", t),
				method:      http.MethodGet,
				clientID:    "logql",
			},
		},
		{
			name: "post request allowed for logs",
			args: args{
				accessToken: fixture("accessToken.json", t),
				method:      http.MethodPost,
				clientID:    "logql",
			},
		},
		{
			// This token only allows logs read but metrics write, so it allows
			// HTTP POST method.
			name: "post request allowed for metrics",
			args: args{
				accessToken: fixture("accessTokenMetrics.json", t),
				method:      http.MethodPost,
				clientID:    "logql",
			},
		},
		{
			name: "post request denied",
			args: args{
				accessToken: fixture("accessTokenRead.json", t),
				method:      http.MethodPost,
				clientID:    "logql",
			},
			wantErr: true,
		},
		{
			name: "wrong client id",
			args: args{
				accessToken: fixture("accessTokenRead.json", t),
				method:      http.MethodGet,
				clientID:    "meh",
			},
			wantErr: true,
		},
		{
			name: "unexpected method",
			args: args{
				accessToken: fixture("accessToken.json", t),
				method:      http.MethodPut,
				clientID:    "logql",
			},
			wantErr: true,
		},
		{
			name: "empty strings",
			args: args{
				accessToken: nil,
				method:      "",
				clientID:    "",
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := AuthorizeMethod(tt.args.accessToken, tt.args.method, tt.args.clientID); (err != nil) != tt.wantErr {
				t.Errorf("authorizeMethod() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
