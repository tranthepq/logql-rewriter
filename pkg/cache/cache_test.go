package cache

import (
	"fmt"
	"testing"
	"time"
)

func Test_requestCache_Put(t *testing.T) {
	type wantFunc func(c *Request, key string) error
	type args struct {
		key  string
		cred Credential
	}
	tests := []struct {
		name string
		args args
		want wantFunc
	}{
		{
			name: "0 time left",
			args: args{
				key:  "test",
				cred: Credential{},
			},
			want: func(c *Request, key string) error {
				time.Sleep(time.Millisecond)
				cred, ok := c.Get(key)
				if !ok {
					return nil
				}
				return fmt.Errorf("got t = %v, expected none", cred)
			},
		},
		{
			name: "enough time left",
			args: args{
				key:  "test",
				cred: Credential{ExpiresAt: time.Now().AddDate(1, 0, 0)},
			},
			want: func(c *Request, key string) error {
				time.Sleep(time.Millisecond)
				_, ok := c.Get(key)
				if !ok {
					return fmt.Errorf("got no value, expected one")
				}
				return nil
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := New()
			c.Put(tt.args.key, tt.args.cred)
			if err := tt.want(c, tt.args.key); err != nil {
				t.Error(err)
			}
		})
	}
}
