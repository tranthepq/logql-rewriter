package main

import "github.com/prometheus/client_golang/prometheus"

const (
	statusSuccess = "success"
	statusFailure = "failure"
)

var (
	tokenLookupDuration = prometheus.NewHistogramVec(
		prometheus.HistogramOpts{
			Name:    "idp_lookup_duration_seconds",
			Help:    "IdP token lookup and verification latency distributions. It includes cache lookup times.",
			Buckets: []float64{.005, .01, .025, .05, .1, .25, .5, 1, 2.5, 5, 10},
		},
		[]string{"method", "status"},
	)
)

func init() {
	prometheus.MustRegister(tokenLookupDuration)
}
